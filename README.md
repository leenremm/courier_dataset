# Courier Font Dataset

## About

Authors: LA Remmelzwaal, G Coppez, B Pitman, M Pitman

Written: October 2019

License: CC-BY-NC

Contact: leenremm [at] gmail [dot] com

## Description

Courier Font Dataset for Neural Network training

10 images for each alphanumeric character (0-9, A-Z, excluding Q). Total of 350 images.

## Reference

To reference this dataset: TODO
